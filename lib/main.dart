import 'package:flame/util.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'box-game.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BoxGame game = BoxGame();
  runApp(game.widget);
  TapGestureRecognizer tapper = TapGestureRecognizer();
  tapper.onTapDown = game.onTapDown;
  runApp(game.widget);
  Util flameUtil = Util();
  await flameUtil.addGestureRecognizer(tapper);
  await flameUtil.fullScreen();
  await flameUtil.setOrientation(DeviceOrientation.portraitUp);
}